public class  Circle implements GeometricObject {
    protected double radius;
    // phương thức khởi tạo 
    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }
    // tính chu vi 
    public double getPerimeter(){
        return radius * 2 * Math.PI;
    }
    // tính diện tích 
    public double getArea(){
        return Math.PI * Math.pow(radius, 2);
    }

    
}
