public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        // khởi tạo 1 hình tròn 
        Circle circle = new Circle(10);
        // khởi tạo hình tròn động
        ResizableCircle resizableCircle = new ResizableCircle(10);  

        // in ra màn hình
        System.out.println("hình tròn 1 ");
        System.out.println(circle.getPerimeter());
        System.out.println(circle.getArea());
        System.out.println("hình tròn động ");
        System.out.println(resizableCircle.getPerimeter());
        System.out.println(resizableCircle.getArea());

        resizableCircle.resSize(150);

        System.out.println("hình tròn động sau khi thay đổi kích thước 150% ");
        System.out.println(resizableCircle.getPerimeter());
        System.out.println(resizableCircle.getArea());



    
    }
}
