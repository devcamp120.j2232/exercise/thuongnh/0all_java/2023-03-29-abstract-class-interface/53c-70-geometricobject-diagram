public class ResizableCircle extends Circle implements Resizable {
    // extends trước và implement ở sau được quy định cho dễ đọc
    // phương thức khởi tạo vòng tròn
    public ResizableCircle(double radius) {
        super(radius);
    }
    // in ra console
    @Override
    public String toString() {
        return "ResizableCircle []";
    }

    // thay đổi kích thước vòng tròn 
    public void resSize(int percent){
        this.radius *= percent/100.0;
    }

    
}
